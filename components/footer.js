import React from 'react';
import { StyleSheet, Text, View} from 'react-native';
import {MaterialIcons} from '@expo/vector-icons';

export default function Footer() {
    return (
        <View style = {styles.footer}>
            <Text style = {styles.text}> <MaterialIcons name='copyright' size={9} color='#c8fff4'/>Bianca 2020</Text>

        </View>
    
    )
}

const styles = StyleSheet.create({

footer:{
 height: 40,
 paddingTop: 0,
 backgroundColor: '#0007',
 borderTopWidth: 3,
 borderTopColor:'#017374',
 padding: 16,
 marginTop: 16,
 flexDirection: 'row',

},

text:{
 textAlign: 'center',
 color: '#c8fff4',
 fontSize: 9,
 flex:1,
 paddingTop:5,
},

})