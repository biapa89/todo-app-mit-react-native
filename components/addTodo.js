import React, {useState} from 'react';
import {StyleSheet, Text, View, TextInput, Button, AsyncStorage} from 'react-native';


export default function AddTodo({submitHandler}) {

    const [text, setText] = useState('');

    // const save = async() => {
    //     try{
    //         await AsyncStorage.setItem("NewTodo", text)
    //     }
    //     catch(err){
    //         alert(err)
    //     }
    // }

    const changeHandler = (val) => {
        setText(val)
    };

     const clearInput = () =>{
         setText('')
     };

    return(
        <View>
            <Text style={styles.headline}>Was gibt´s zu tun?</Text>
            <TextInput
            style ={styles.input}
            autoCorrect={true}
            placeholder='neues Todo...'
            value={text}
            onChangeText={(val) => changeHandler(val)}
            />
            <Button onPress={() => {submitHandler(text); clearInput()} }  title='Todo hinzufügen' color='#03DAC5'/>
        </View>
    )
}

const styles = StyleSheet.create ({

        input: {
            marginBottom: 20,
            paddingHorizontal: 8,
            paddingVertical: 6,
            borderWidth: 1,
            borderColor: '#c8fff4',
            alignSelf:"stretch",
            height:45,
            borderRadius:6,
            color:'#C8FFF4'
        
        },

        headline:{
            color:'#c8fff4',
            fontSize: 20,
            textAlign:'center',
            paddingBottom: 10
          },
}) 

