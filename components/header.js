import React from 'react';
import { StyleSheet, Text, View} from 'react-native';
import {MaterialIcons} from '@expo/vector-icons';

export default function Header() {
    return (
        <View style = {styles.header}>
            <MaterialIcons name='home' size={18} color='#c8fff4'/>
            <Text style = {styles.title}>Meine ToDo's</Text>
            <MaterialIcons name='menu' size={18} color='#c8fff4'/>
        </View>
    
    )
}

const styles = StyleSheet.create({

header:{
 height: 80,
 paddingTop: 38,
 backgroundColor: '#0007',
 borderBottomWidth: 3,
 borderBottomColor:'#017374',
 padding: 16,
 marginTop: 16,
 flexDirection: 'row',

},

title:{
 textAlign: 'center',
 color: '#c8fff4',
 fontSize: 20,
 fontWeight: 'bold',
 marginLeft: 60,
 marginRight:60
},

})