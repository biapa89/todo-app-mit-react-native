import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {MaterialIcons} from '@expo/vector-icons';

export default function TodoItem ({item, pressHandler}){
    return(
        <TouchableOpacity onPress={() => pressHandler(item.key)}>
            <View style={styles.item}>
                <MaterialIcons name='clear' size={18} color='#c8fff4'/>
            <Text style={styles.itemText}>{item.text}</Text>

            </View>
        </TouchableOpacity>
    )

}

const styles = StyleSheet.create ({

    item:{
        padding: 16,
        marginTop: 16,
        borderColor: '#CF6678',
        borderWidth: 1,
        borderStyle: 'solid',
        borderRadius: 10,
        flexDirection: 'row',

    },

    itemText:{
        marginLeft: 10,
        color: '#c8fff4',
    }
})