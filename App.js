import React, {useState} from 'react';
import { StyleSheet, Text, View, FlatList, Alert, TouchableWithoutFeedback, Keyboard} from 'react-native';

import Header from './components/header';
import TodoItem from './components/todoItem';
import AddTodo from './components/addTodo';
import Footer from './components/footer';



export default function App() {                // root-Component der App, also quasi das, was auf der "Startseite" angezeigt wird
  
const [todos, setTodos] = useState([
  {text: 'JavaScript wiederholen', key: '1'},
  {text: 'Idee für Abschlussprojekt', key: '2'},
  
]);




const pressHandler = (key) => {
  setTodos((prevTodos) => {
    return prevTodos.filter(todo => todo.key != key);
  });
}

// const defaultText = () =>{
//   setText('')
// }

const submitHandler= (text) => {

  if (text.length > 3) {
    setTodos((prevTodos) =>{
      return [
        {text: text, key: Math.random().toString()},
        ...prevTodos
      ]
    })
  }
  else {
    Alert.alert('Ups!', 'Dein Todo muss länger als 3 Buchstaben sein', [
      {text: 'Verstanden'}
    ]) 
  }

 }
  return (
    
    <TouchableWithoutFeedback onPress={() => {
      Keyboard.dismiss()
    }}>
      <View style={styles.container}>

        <Header />

        <View style={styles.content}>

          <AddTodo submitHandler={submitHandler} />

          <View style={styles.list}>

            <Text style={styles.open}>Offene ToDo´s</Text>
            <Text style={styles.line}>______________________</Text>

            <FlatList
              data={todos}
              renderItem={({ item }) => (
                <TodoItem item={item} pressHandler={pressHandler} />
              )}
            />

          </View>

        </View>

          <Footer/>

      </View>
    </TouchableWithoutFeedback>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#323131',
  },


  content:{
    padding: 40,
    flex: 1,
  },

  list:{
    flex: 1,
    marginTop: 20,
  },
  
  open:{
    color:'#CF6679',
    fontSize: 16,
    fontWeight:'bold',
    textAlign:'center',
    paddingTop: 5,
    paddingBottom: 1
  },

  line:{
    textAlign:'center',
    color:'#CF6679'

  }

  // done:{
  //   color:'#2AA57C',
  //   fontSize: 16,
  //   textAlign:'center',
  //   paddingTop: 20,
  //   paddingBottom: 10
  // }
  

 
});
